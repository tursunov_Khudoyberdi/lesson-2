// First task
const devisible3And5 = a => {
  if (a % 3 === 0 && a % 5 === 0) {
    return `${a} is divisible by both 3 and 5.`;
  } else {
    return `${a} is not divisible by 3 and 5`;
  }
};

console.log(devisible3And5(15));

// Second task
const evenOrOdd = b => {
  if (b % 2 === 0) {
    return `${b} is an even number.`;
  } else {
    return `${b} is an odd number.`;
  }
};

console.log(evenOrOdd(5));

// Third task
const iSortArray = a => a.sort((a, b) => a - b);

console.log(iSortArray([23, 54, 31, 16, 74, 82, 11, 42]));
